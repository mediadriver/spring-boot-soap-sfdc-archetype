#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// CXF parameters
	@Value("${symbol_dollar}{cxf.servlet.context}")
	private String cxfServletContext;

	@Value("${symbol_dollar}{cxf.service.address}")
	private String cxfServiceAddres;

	@Value("${symbol_dollar}{cxf.service.options}")
	private String cxfServiceOptions;

	// WS-Security Endpoint parameters
	@Value("${symbol_dollar}{auth.verification.userName}")
	private String verificationUserName;

	@Value("${symbol_dollar}{auth.verification.password}")
	private String verificationPassword;

	// Salesforce details
	@Value("${symbol_dollar}{sfdc.loginUrl}")
	private String sfdcLoginUrl;

	@Value("${symbol_dollar}{sfdc.clientId}")
	private String sfdcClientId;

	@Value("${symbol_dollar}{sfdc.clientSecret}")
	private String sfdcClientSecret;

	@Value("${symbol_dollar}{sfdc.userName}")
	private String sfdcUserName;

	@Value("${symbol_dollar}{sfdc.password}")
	private String sfdcPassword;

	// Route details
	@Value("${symbol_dollar}{salesforce.action}")
	private String salesforceAction;

	public String getCxfServletContext() {
		return cxfServletContext;
	}

	public void setCxfServletContext(String cxfServletContext) {
		this.cxfServletContext = cxfServletContext;
	}

	public String getCxfServiceAddres() {
		return cxfServiceAddres;
	}

	public void setCxfServiceAddres(String cxfServiceAddres) {
		this.cxfServiceAddres = cxfServiceAddres;
	}

	public String getCxfServiceOptions() {
		return cxfServiceOptions;
	}

	public void setCxfServiceOptions(String cxfServiceOptions) {
		this.cxfServiceOptions = cxfServiceOptions;
	}

	public String getVerificationUserName() {
		return verificationUserName;
	}

	public void setVerificationUserName(String verificationUserName) {
		this.verificationUserName = verificationUserName;
	}

	public String getVerificationPassword() {
		return verificationPassword;
	}

	public void setVerificationPassword(String verificationPassword) {
		this.verificationPassword = verificationPassword;
	}

	public String getSfdcLoginUrl() {
		return sfdcLoginUrl;
	}

	public void setSfdcLoginUrl(String sfdcLoginUrl) {
		this.sfdcLoginUrl = sfdcLoginUrl;
	}

	public String getSfdcClientId() {
		return sfdcClientId;
	}

	public void setSfdcClientId(String sfdcClientId) {
		this.sfdcClientId = sfdcClientId;
	}

	public String getSfdcClientSecret() {
		return sfdcClientSecret;
	}

	public void setSfdcClientSecret(String sfdcClientSecret) {
		this.sfdcClientSecret = sfdcClientSecret;
	}

	public String getSfdcUserName() {
		return sfdcUserName;
	}

	public void setSfdcUserName(String sfdcUserName) {
		this.sfdcUserName = sfdcUserName;
	}

	public String getSfdcPassword() {
		return sfdcPassword;
	}

	public void setSfdcPassword(String sfdcPassword) {
		this.sfdcPassword = sfdcPassword;
	}

	public String getSalesforceAction() {
		return salesforceAction;
	}

	public void setSalesforceAction(String salesforceAction) {
		this.salesforceAction = salesforceAction;
	}

}
